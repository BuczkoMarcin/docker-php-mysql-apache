# docker-php-mysql-apache

Git:
https://bitbucket.org/BuczkoMarcin/docker-php-mysql-apache/src/master/

Konfiguracja prostego środowiska dockerowego z Apache, mysql 8.0, php 7.4 i phpmyAmin.

## Konfiguracja

- Hasła do bazy danych i nazwa bazy danych są skonfigurowane w pliku  .env.

- W przypadku uruchamiania na windows 10 Home + dockerToolBox + VirtualBox lub w przypadku wystapienia bledu:

```bash
    mysql8  ... Operating system error number 22 in a file operation.
    mysql8  ... Error number 22 means 'Invalid argument'  
```

nalezy odkomentowac w pliku docker-compose.yml linie:

```docker
    db:
        container    _name: mysql8
        image: my    sql:8.0
        command:     fault-authentication-plugin=mysql_native_password
        restart: always
        volumes:    
            - ./mysql/db:/var/lib/mysql/    
        # odkomentuj w windows 10 home + virtualbox + docker     box    
        #   - ./mysql/conf.d:/etc/mysql/conf.d
``` 

Plikowi mysql/conf.d/local.cnf nalezy nadać odpowiednie uprawnienia. Inaczej jest ignorowany przez mysqld. 

```bash
    $ chmod 0400 mysql/conf.d/local.cnf
```

## Użycie

- W katalogu www umieszczamy źródła aplikacji PHP. 
- Katalogu mysql/db Będzie służył do przechowywania danych bazy danych. W ten sposób po zatrzymaniu kontenerów dane z bazy danych zostaną zachowane i baza nie zostanie zmieniona po ponownym ich uruchomieniu.
- Urychamiamy: ```docker-compose up -d --build```
- aplikacja php widoczna pod adresem: http://localhost:8000
- phpmyadmin z dostępem do bazy danych widoczny pod adresem http://localhost:8001
- w celu zatrzymania kontenerów i ich skasowania ```docker-compose down```

### Źródła
- https://www.youtube.com/watch?v=_mwWxgfZ7Zc
- https://phoenixnap.com/kb/mysql-docker-container
